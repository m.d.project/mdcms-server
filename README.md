## Install

> Note the an UID and GID used on your system for the www user.
>
> Is important tu set correct users in .env file before installation.

First create preconfigured .env file:

```
./preset-env {project_domain} {app_git_clone_link} {access_token}
```

Where {project_domain} is like: ```mydomain.com``` and {app_git_clone_link} include access token like: ```https://user:access_key@gitlab.com/xyz/xyz.git```.
```access_token``` is gitlab token with read api permission. This token is used for composer during cms install proccess.

Check ```.env``` file and if is correct run:

```
./install
```

After installation, check app/.env file and make adjustments if necessary.

## Add admin user

After installation you should add admin account used to login to the panel.

```
./add-admin-user {name} {email} {password}
```

## Run

```
sudo docker compose up
```

For standalone - not for webproxy - version run:

```
sudo docker compose -f ./docker-compose-standalone.yml up
```

## Tools

### App bash console

```
./app-bash
```

### App update

```
./app-update
```

if you don't want to compole assets (npm) use --skip-assets flag:

```
./app-update --skip-assets
```

### Database console

```
./app-database
```

### Autostart

If script path is other than the default, change before copy.

```
sudo cp mdcms-server.service /etc/systemd/system/
sudo systemctl start mdcms-server && sudo systemctl enable mdcms-server
```

### Backup

During backup create and restore, database docker service must be online.

#### Create backup

```
./make-backup
```

Will create a backup with the date as the name. If you want to set your own name, give it as an argument:

```
./make-backup copy_1
```

Backup creates a copy of app database and public storeage files.

#### Restore backup

```
./restore-backup backup_name
```

#### Auto backup

Example for weekly backup.
Add to root crontab:
```
59 23 * * * /project_path/make-backup $(date +\%A) >> /dev/null
```

#### Auto update

```
* * * * * /project_path/app-update-if-new-commits >> /dev/null
```

#### Scheduler

```
* * * * * /project_path/app-schedule-run >> /dev/null
```
